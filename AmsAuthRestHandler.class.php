<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAuthRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAuthRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("UserModel.class.php");
require_once("AirlineModel.class.php");
require_once("AirportModel.class.php");
require_once("RunwayModel.class.php");

/**
 * Description of AmsAuthRestHandler
 *
 * @author IZIordanov
 */
class AmsAuthRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "AmsAuthRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new AmsAuthRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "AmsAuthRestHandler::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            if (isset($conn)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="JWT Tocken Methods">
    
    public function Login($email, $password, $location)
    {
        $mn = "AmsAuthRestHandler::Login()";
        AmsAlLogger::logBegin($mn);
        //AmsAlLogger::log($mn, " eMail: " . $email.", password".$password); 
        $response = null;
        try {
            $user = AmsUser::login($email, $password);
            if (isset($user)) {
                //AmsAlLogger::log($mn, " user = " . json_encode($user));
                $ipAddress = $_SERVER['REMOTE_ADDR']; 
                if(isset($location)){
                    $location->ipAddress = $ipAddress;
                    $location->action = "user login";
                    $location->time = CurrenDateTime();
                } else{
                    $location = $ipAddress;
                }
                $user->ipAddress = $location;
                $user = AmsUser::Save($user);
                if (JwtAuth::signTocken($user->userId, $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("user", $user);
                    
                    if(isset($user) && isset($user->userId)){
                        $settings = AmsUserSettings::LoadById($user->userId);
                        $response->addData("settings", $settings);
                        $airline = AirlineModel::LoadByUserId($user->userId);
                        if(isset($airline)){
                            $response->addData("airline", $airline);
                            if(isset($airline->apId)){
                                $hqAirport = AmsAirport::LoadById($airline->apId);
                                if(isset($hqAirport)){
                                    $response->addData("hqAirport", $hqAirport);

                                }
                            }
                        }
                    }
                    
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                $response = new Response("error", "Invalid Credentioals.");
                $response->statusCode = 401;
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function refreshToken($refresh)
    {
        $mn = "AmsAuthRestHandler::refreshToken()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $user = JwtAuth::RefreshTocken($refresh);
             if (isset($user)) {
                AmsAlLogger::log($mn, " user = " . $user->toJson());
                if (JwtAuth::signTocken($user->userId, $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("currUser", $user);
                    $settings = AmsUserSettings::LoadById($user->userId);
                    $response->addData("currUserSettings", $settings);
                    $airline = AirlineModel::LoadByUserId($user->userId);
                    if(isset($airline)){
                        $response->addData("airline", $airline);
                         if(isset($airline->apId)){
                            $hqAirport = AmsAirport::LoadById($airline->apId);
                            if(isset($hqAirport)){
                                $response->addData("hqAirport", $hqAirport);

                            }
                        }
                    }
                    
                    
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 412;
                }
            } else {
                $response = new Response("error", "Invalid Refresh Token.");
                $response->statusCode = 412;
            }
            

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="User">
    
    public function Register($eMail, $password, $location)
    {
        $mn = "AmsAuthRestHandler::register()";
        AmsAlLogger::logBegin($mn);
        AmsAlLogger::log($mn, " eMail: " . $eMail.", password".$password); 
        $response = null;
        $errMsg = null;
        try {
        
            $wrongEmail = FALSE;
            
            if(!isset($eMail) || !isset($password)){
                $wrongEmail = TRUE;
                $errMsg = "Missing required parameter.";
            }
            
            if(((bool)$wrongEmail) == FALSE){
                $wrongEmail = !checkEmail($eMail);
                AmsAlLogger::log($mn, "checkEmail = " . $wrongEmail?'true':'false');
                if(((bool)$wrongEmail)){
                     $errMsg = "E-Mail value not valid.";
                }
            }
            
            if(((bool)$wrongEmail) == FALSE){
                $conn = AmsAlConnection::dbConnect();
                $logModel = AmsAlLogger::currLogger()->getModule($mn);
                
                $check = AmsUser::CheckEmailJson($eMail, $conn, $mn, $logModel);
                $valJson = json_encode($check[0]);
                $val = json_decode($valJson);
                //AmsAlLogger::log($mn, " check = " . json_encode($val));
                $rows = (int)$val->rows;
                
                AmsAlLogger::log($mn, " rows = " . $rows);
                $wrongEmail = (($val->rows > 0)?true:false);
                if(((bool)$wrongEmail)){
                     $errMsg = "E-Mail already registered.";
                }
            }
            
            AmsAlLogger::log($mn, " wrongEmail= " . $wrongEmail);
            if (((bool)$wrongEmail) == FALSE) {
                $payload = [
                    'userEmail' => $eMail, // Issued at: time when the token was generated
                    'userPwd' => $password, 
                    'ipAddress'=> $location,
                    ];
                AmsAlLogger::log($mn, " payload = " . json_encode($payload));
                $dataJson = json_encode($payload);
                $val = json_decode($dataJson);
                $user = AmsUser::Save($val);
                
                AmsAlLogger::log($mn, "new user = " . json_encode($user));
                 if (JwtAuth::signTocken($user->userId, $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("currUser", $user);
                    $settings = AmsUserSettings::LoadById($user->userId);
                    $response->addData("currUserSettings", $settings);
                    $airline = AirlineModel::LoadByUserId($user->userId);
                    if(isset($airline)){
                        $response->addData("airline", $airline);
                    }
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                if(!isset($errMsg))
                {
                    $errMsg = "Invalid values provided";
                }
                $response = new Response("error", $errMsg);
                $response->statusCode = 200;
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function AmsUserSave($value) {
        $mn = "AmsAuthRestHandler::AmsUserSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $user = AmsUser::Save($value);
            $response = new Response("success", "User data saved.");
            $response->addData("user", $user);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AmsUserSettings($id) {
        $mn = "AmsAuthRestHandler::AmsUserSettings()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsUserSettings::LoadById($id);
            $response = new Response("success", "User settings get");
            $response->addData("settings", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    
    
     // </editor-fold>
}
