<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAircraftRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAircraftRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("UserModel.class.php");
require_once("CountryModel.class.php");
require_once("StateModel.class.php");
require_once("AirportModel.class.php");
require_once("RunwayModel.class.php");
require_once("ManufacturerModel.class.php");
require_once("MacModel.class.php");
require_once("MacCabinModel.class.php");
require_once("AircraftModel.class.php");



/**
 * Description of AmsAircraftRestHandler
 *
 * @author IZIordanov
 */
class AmsAircraftRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "AmsAircraftRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new AmsAircraftRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "AmsAircraftRestHandler::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            if (isset($conn)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    
    public function GetJsonData($params) {
        $mn = "GetJsonData()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = $params->sql;
            $sql .= " LIMIT ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["i", 1000];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("jsondata", $ret_json_data);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Manufacturer">
    
    public function ManufacturerGet($value) {
        $mn = "AmsAircraftRestHandler::ManufacturerGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = Manufacturer::LoadById($value);
            $response = new Response("success", "Mfr get.");
            $response->addData("manufacturer", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function ManufacturerSave($value) {
        $mn = "AmsAircraftRestHandler::ManufacturerSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = Manufacturer::Save($value);
            $response = new Response("success", "Mfr data saved.");
            $response->addData("manufacturer", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function ManufacturerDelete($mfr_id) {
        $mn = "AmsAircraftRestHandler::ManufacturerDelete(".$mfr_id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = Manufacturer::Delete($mfr_id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Airport deleted.");
            $response->addData("mfrId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="MAC">
    
    public function MacGet($value) {
        $mn = "AmsAircraftRestHandler::MacGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = Mac::LoadById($value);
            $response = new Response("success", "mac get.");
            $response->addData("mac", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function MacSave($value) {
        $mn = "AmsAircraftRestHandler::MacSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = Mac::Save($value);
            $response = new Response("success", "Mfr data saved.");
            $response->addData("mac", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function MacDelete($id) {
        $mn = "AmsAircraftRestHandler::MacDelete(".$id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = Mac::Delete($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "MAC deleted.");
            $response->addData("macId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Aircraft">
    
    public function AircraftGet($value) {
        $mn = "AmsAircraftRestHandler::AircraftGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AircraftModel::LoadById($value);
            $response = new Response("success", "ac get.");
            $response->addData("aircraft", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="MAC Cabin">
    
    public function MacCabinGet($value) {
        $mn = "AmsAircraftRestHandler::MacCabinGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = MacCabin::LoadById($value);
            $response = new Response("success", "mac get.");
            $response->addData("cabin", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function MacCabinSave($value) {
        $mn = "AmsAircraftRestHandler::MacCabinSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = MacCabin::Save($value);
            $response = new Response("success", "Mfr data saved.");
            $response->addData("cabin", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function MacCabinDelete($id) {
        $mn = "AmsAircraftRestHandler::MacCabinDelete(".$id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = MacCabin::Delete($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "MAC Cabin deleted.");
            $response->addData("cabinId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
}
