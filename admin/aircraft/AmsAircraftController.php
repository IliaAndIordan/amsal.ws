<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : AmsAircraftController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/amsal' . $delim . '/home/iordanov/common/lib/amsal/common' . 
    $delim .'/home/iordanov/common/lib/amsal/al-shedule' . 
    $delim . '/home/iordanov//iordanov.info/amsal/ws'.
    $delim . '/home/iordanov//iordanov.info/amsal/ws/admin'.
    $delim . '/home/iordanov//iordanov.info/amsal/ws/aircraft'.
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "AmsAircraftController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("AmsAircraftRestHandler.class.php");
require_once("UserModel.class.php");
require_once("CountryModel.class.php");
require_once("StateModel.class.php");
require_once("AirportModel.class.php");
require_once("RunwayModel.class.php");
require_once("ManufacturerModel.class.php");
require_once("MacModel.class.php");
require_once("MacCabinModel.class.php");
require_once("AircraftModel.class.php");

AmsAlLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
AmsAlLogger::log($mn, "view=" . $view);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new AmsAircraftRestHandler();
    $restHendler->Option();
    AmsAlLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    
    $payloadJson;
    $payloadAuth;
    
    $authRes = JwtAuth::Autenticate();
    
    if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
            
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new AmsAircraftRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    }
    else{
        $response = new Response("error", 'Autentication is required');
        $response->statusCode = 401;
        $rh = new AmsAircraftRestHandler();
        $rh->EncodeResponce($response);
        return;
    }
    //AmsAlLogger::log($mn, "payloadAuth: " . json_encode($payloadAuth) . " ");
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAircraftRestHandler();
            $restHendler->Ping($id);
            AmsAlLogger::log($mn, "ping executed");
            break;
        case "get_json":
            // to handle REST Url /pcpd/
            $rh = new AmsAircraftRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                AmsAlLogger::log($mn, " sql: " . $payload_json->sql . " ");
                if (isset($payload_json)){
                    $response = $rh->GetJsonData($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
        // <editor-fold defaultstate="collapsed" desc="Manufacturer">
        
        case "mfr_table":
            // to handle REST Url /pcpd/
            $rh = new AmsAircraftRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = Manufacturer::ManufacturerTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "mfr_save":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->ManufacturerSave($dataJson->manufacturer);
            }
            break;
        
        case "mfr_delete":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->ManufacturerDelete($dataJson->mfrId);
            }
            break;
            
        case "mfr_get":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->ManufacturerGet($dataJson->apId);
            }
            break;
            
        // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="MAC">
        
        case "mac_table":
            // to handle REST Url /pcpd/
            $rh = new AmsAircraftRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = Mac::MacTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "mac_save":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->MacSave($dataJson->mac);
            }
            break;
        
        case "mac_delete":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->MacDelete($dataJson->macId);
            }
            break;
            
        case "mac_get":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->MacGet($dataJson->macId);
            }
            break;
        
        case "mac_market_table":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = Mac::MacMarketTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="AmsAircraft">
        case "acat_purchase":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            $dataJson = json_decode($payload);
            if (isset($dataJson)){
                $response = AircraftModel::BuyId($dataJson->acId, $dataJson->alId);
                $rh->EncodeResponce($response);
                return;
            } else{
                $response = new Response("error", 'Missing required parameters.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="AC Maintenance">

        case "acmp_table":
            $rh = new AmsAircraftRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AircraftModel::MaintenancePlanTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "acmp_create":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $response = AircraftModel::MaintenancePlanCreate($dataJson->acId, $dataJson->amtId, $dataJson->stTime);
                $rh->EncodeResponce($response);
                return;
            }
            else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
        
         // </editor-fold>
                
        // <editor-fold defaultstate="collapsed" desc="MAC Cabin">
        
        case "mac_cabin_table":
            // to handle REST Url /pcpd/
            $rh = new AmsAircraftRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = MacCabin::MacCabinTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "mac_cabin_save":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->MacCabinSave($dataJson->cabin);
            }
            break;
        
        case "mac_cabin_delete":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->MacCabinDelete($dataJson->cabinId);
            }
            break;
            
        case "mac_cabin_get":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->MacCabinGet($dataJson->cabinId);
            }
            break;
            
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Aircrafts">
        
        case "ac_table":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AircraftModel::AircraftTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "ac_get":
            $rh = new AmsAircraftRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AircraftGet($dataJson->acId);
            }
            break;
        // </editor-fold>
        
        default:
            AmsAlLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


AmsAlLogger::logEnd($mn);

