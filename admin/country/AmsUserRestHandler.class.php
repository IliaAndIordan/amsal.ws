<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsUserRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsUserRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("UserModel.class.php");

/**
 * Description of AmsUserRestHandler
 *
 * @author IZIordanov
 */
class AmsUserRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "AmsUserRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new AmsUserRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "AmsUserRestHandler::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            if (isset($conn)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Charts">
    
    public function Login($email, $password, $location)
    {
        $mn = "AmsUserRestHandler::Login()";
        AmsAlLogger::logBegin($mn);
        AmsAlLogger::log($mn, " eMail: " . $eMail.", password".$password); 
        $response = null;
        try {
            $user = AmsUser::Login($email, $password);
            if (isset($user)) {
                AmsAlLogger::log($mn, " user = " . $user->toJson());
                $user->ipAddress = $location;
                $user = AmsUser::Save(json_encode($user));
                if (JwtAuth::signTocken($user->userId, $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("user", $user);
                    
                    if(isset($user) && isset($user->userId)){
                        $settings = AmsUserSettings::LoadById($user->userId);
                        $response->addData("settings", $settings);
                    }
                    
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                $response = new Response("error", "Invalid Credentioals.");
                $response->statusCode = 401;
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function refreshToken($refresh)
    {
        $mn = "AmsUserRestHandler::refreshToken()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $user = JwtAuth::RefreshTocken($refresh);
             if (isset($user)) {
                AmsAlLogger::log($mn, " user = " . $user->toJson());
                if (JwtAuth::signTocken($user->userId, $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("currUser", $user);
                    $settings = AmsUserSettings::LoadById($user->userId);
                    $response->addData("currUserSettings", $com);
                    
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 412;
                }
            } else {
                $response = new Response("error", "Invalid Refresh Token.");
                $response->statusCode = 412;
            }
            

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="SxUser">
    
    function LoginsCountSeventDaysJson(){
        $mn = "AmsUserRestHandler::LoginsCountSeventDaysJson()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
             $sql = "SELECT count(uh.id) as logins, count(distinct(uh.user_id)) as users, 
                 DATE(uh.udate) logdate 
                FROM ams_al.ams_user_h uh
                where uh.udate > DATE_ADD( CURDATE(), INTERVAL -7 DAY) and 1=?
                group by DATE(uh.udate)" ;

            $bound_params_r = ["i",1];

            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("rows", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM ams_al.ams_user u where 1=?"  ;
            $bound_params_r = ["i", 1];
            $n_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $n_json_data);
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    
    
     // </editor-fold>
}
