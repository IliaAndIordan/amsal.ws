<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : AmsCountryController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/amsal' . $delim . '/home/iordanov/common/lib/amsal/common' . 
    $delim .'/home/iordanov/common/lib/amsal/al-shedule' . 
    $delim . '/home/iordanov//iordanov.info/amsal/ws'.
    $delim . '/home/iordanov//iordanov.info/amsal/ws/admin'.
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "AmsCountryController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("AmsCountryRestHandler.class.php");
require_once("UserModel.class.php");
require_once("CountryModel.class.php");
require_once("StateModel.class.php");
require_once("CityModel.class.php");
require_once("AmsTask.class.php");

AmsAlLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
AmsAlLogger::log($mn, "view=" . $view);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new AmsCountryRestHandler();
    $restHendler->Option();
    AmsAlLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    
    $payloadJson;
    $payloadAuth;
    
    $authRes = JwtAuth::Autenticate();
    
    if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new AmsCountryRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    }
    else{
        $response = new Response("error", 'Autentication is required');
        $response->statusCode = 401;
        $rh = new AmsCountryRestHandler();
        $rh->EncodeResponce($response);
        return;
    }
    
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new AmsCountryRestHandler();
            $restHendler->Ping($id);
            AmsAlLogger::log($mn, "ping executed");
            break;
        
        case "regions":
            // to handle REST Url /pcpd/
            $restHendler = new AmsCountryRestHandler();
            $restHendler->RegionsJson();
            break;
        
        // <editor-fold defaultstate="collapsed" desc="Country">
        
        case "country_table":
            // to handle REST Url /pcpd/
            $rh = new AmsCountryRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsCountry::CountryTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "country_save":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AmsCountrySave($dataJson->country);
            }
            break;
            
        // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="State">
        
        case "state_table":
            // to handle REST Url /pcpd/
            $rh = new AmsCountryRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsState::StateTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
         case "state_get":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AmsStateGet($dataJson->stateId);
            }
            break;
        
        case "state_save":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AmsStateSave($dataJson->state);
            }
            break;
        
        case "state_delete":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AmsStateDelete($dataJson->stateId);
            }
            break;
            
        // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="City">
        
        case "city_table":
            // to handle REST Url /pcpd/
            $rh = new AmsCountryRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsCity::CityTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "city_save":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AmsCitySave($dataJson->city);
            }
            break;
        case "city_get":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AmsCityGet($dataJson->cityId);
            }
            break;
        
        case "city_delete":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AmsCityDelete($dataJson->cityId);
            }
            break;
            
        // </editor-fold>
           
        // <editor-fold defaultstate="collapsed" desc="City Pax and Cargo Demand">
        
        case "count_city_no_demand":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            AmsAlLogger::log($mn, " count_city_no_demand");
            
            $response = AmsCity::CountCityNoPaxDemend();
            $rh->EncodeResponce($response);
        break;  
        case "calc_city_payload_demand":
            $rh = new AmsCountryRestHandler();
            $payload = file_get_contents('php://input');
            AmsAlLogger::log($mn, " calc_city_payload_demand");
            
            $response = AmsCity::CalcCityPayloadDemand();
            $rh->EncodeResponce($response);
        break;  
    
    
        // </editor-fold>
    
        // <editor-fold defaultstate="collapsed" desc="City Pax and Cargo Demand">
        
        case "admin_task_table":
            $rh = new AmsCountryRestHandler();
            //AmsAlLogger::log($mn, " count_city_no_demand");
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsTask::TaskTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;  
        // </editor-fold>
            
        default:
            AmsAlLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


AmsAlLogger::logEnd($mn);

