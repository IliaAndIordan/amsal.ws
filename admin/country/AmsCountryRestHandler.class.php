<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsCountryRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsCountryRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("UserModel.class.php");
require_once("CountryModel.class.php");
require_once("StateModel.class.php");
require_once("CityModel.class.php");

/**
 * Description of AmsCountryRestHandler
 *
 * @author IZIordanov
 */
class AmsCountryRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "AmsCountryRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new AmsCountryRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "AmsCountryRestHandler::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            if (isset($conn)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Regions">
    
    function RegionsJson(){
        $mn = "AmsCountryRestHandler::RegionsJson()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
             $sql = "SELECT r.region_id as regionId, r.region_code as rCode, 
                        r.region_name as rName, r.wiki_link as rWikiUrl, 
                        count(distinct(sr.subregion_id)) as subregions,
                        count(distinct(c.country_id)) as countries,
                        count(distinct(cs.state_id)) as states,
                        count(distinct(a.ap_id)) as airports,
                        count(distinct(aa.ap_id)) as apActive
                   FROM ams_wad.cfg_region r
                   left join ams_wad.cfg_subregion sr on sr.region_id = r.region_id
                   left join ams_wad.cfg_country c on c.region_id = r.region_id
                   left join ams_wad.cfg_country_state cs on cs.country_id = c.country_id
                   left join ams_wad.cfg_airport a on a.region_id = r.region_id and a.country_id = c.country_id and a.state_id = cs.state_id
                   left join ams_wad.cfg_airport aa on aa.region_id = r.region_id and aa.country_id = c.country_id and aa.state_id = cs.state_id and aa.ap_active = 1
                    where 1= ?
                   group by r.region_id
                   order by r.region_name" ;

            $bound_params_r = ["i",1];

            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("regions", $ret_json_data);
            
            $sql = "SELECT sr.subregion_id as subregionId,
                    sr.subregion_code as srCode,
                    sr.subregion_name as srName,
                    sr.wiki_link as srWikiUrl,
                    sr.region_id as regionId,
                    sr.image_url as srImageUrl,
                    count(distinct(c.country_id)) as countries,
                    count(distinct(cs.state_id)) as states,
                    count(distinct(a.ap_id)) as airports,
                    count(distinct(aa.ap_id)) as apActive
                   FROM ams_wad.cfg_subregion sr
                   left join ams_wad.cfg_country c on c.subregion_id = sr.subregion_id
                   left join ams_wad.cfg_country_state cs on cs.country_id = c.country_id
                   left join ams_wad.cfg_airport a on a.region_id = sr.region_id and a.country_id = c.country_id and a.state_id = cs.state_id
                   left join ams_wad.cfg_airport aa on aa.region_id = sr.region_id and aa.country_id = c.country_id and aa.state_id = cs.state_id and aa.ap_active = 1
                   where 1= ?
                   group by sr.subregion_id
                   order by sr.region_id, sr.subregion_name"  ;
            $bound_params_r = ["i", 1];
            $n_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("subregions", $n_json_data);
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Country">
    
    public function AmsCountrySave($value) {
        $mn = "AmsCountrySave::AmsUserSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsCountry::Save($value);
            $response = new Response("success", "Country data saved.");
            $response->addData("country", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="State">
    
    public function AmsStateGet($value) {
        $mn = "AmsCountryRestHandler::AmsStateGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsState::LoadById($value);
            $response = new Response("success", "State get.");
            $response->addData("state", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AmsStateSave($value) {
        $mn = "AmsCountryRestHandler::AmsStateSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsState::Save($value);
            $response = new Response("success", "State data saved.");
            $response->addData("state", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AmsStateDelete($state_id) {
        $mn = "AmsCountryRestHandler::AmsStateDelete(".$state_id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = AmsState::Delete($state_id, $conn, $mn, $logModel);
            
            $response = new Response("success", "State deleted.");
            $response->addData("stateId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="City">
    
    public function AmsCitySave($value) {
        $mn = "AmsCountryRestHandler::AmsCitySave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsCity::Save($value);
            $response = new Response("success", "City data saved.");
            $response->addData("city", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AmsCityGet($value) {
        $mn = "AmsCountryRestHandler::AmsCityGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsCity::LoadById($value);
            $response = new Response("success", "City get.");
            $response->addData("city", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AmsCityDelete($city_id) {
        $mn = "AmsCountryRestHandler::AmsCityDelete(".$city_id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = AmsCity::Delete($city_id, $conn, $mn, $logModel);
            
            $response = new Response("success", "City deleted.");
            $response->addData("cityId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
}
