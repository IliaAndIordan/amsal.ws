<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : SolrClient.class.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("Functions.php");
require_once("UserModel.class.php");
require_once("CountryModel.class.php");
require_once("StateModel.class.php");
require_once("AirportModel.class.php");
require_once('Apache2/Solr/Service.php');
require_once('Apache2/Solr/HttpTransport/Curl.php');

/**
 * Description of SolrClient
 *
 * @author IZIordanov
 */
class AmslSolrClient extends SimpleRest{

    // <editor-fold defaultstate="collapsed" desc="Constructors">
    private $transportInstance;
    private $httpVersion = "HTTP/1.1";
    private $config_core_airports = array(
        'endpoint' => array(
            'localhost' => array(
                'host' => DB_HOST,
                'port' => SOLR_PORT,
                'path' => '/solr/amswad/'
            )
        )
    );
    private $solr_core_airports = null;
    private $mn;

    public function __construct() {

        if (!isset($this->mn))
            $this->mn = "SolrClient";
        // create a new solr service instance - host, port, and webapp
        // path (all defaults in this example)
        $this->transportInstance = new Apache_Solr_HttpTransport_Curl();
        $this->solr_core_airports = new Apache_Solr_Service(DB_HOST, SOLR_PORT, '/solr/amswad/', $this->transportInstance);
        try{
             $pingResp = $this->solr_core_airports->ping();
             
            // AmsAlLogger::log($this->mn, " ping resp = " . serialize($pingResp));
        } catch (Exception $ex) {
            AmsAlLogger::logError($this->mn, $ex);
        }
        if (!$pingResp) {
            AmsAlLogger::log($this->mn, "WARNING: Solr solr_core_airports service not responding!");
            //exit;
        }
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Airports">
    
    public function Ping()
    {
        $mn = "SolrClient::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            if (!isset($this->solr_core_airports)) {
                $this->solr_core_airports = new Apache_Solr_Service(DB_HOST, SOLR_PORT, '/solr/amswad/');
            }
            $pingResp = $this->solr_core_airports->ping();
           
            if (isset($pingResp)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Solr amswad ping is working.");
                $response->addData("ping", $pingResp);
            } else {
                $response = new Response("success", "There is something wrong in ping solr amswad.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function Autocomplete($criteria) {
       
        $mn = "SolrClient::Autocomplete()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        
        if (!isset($this->solr_core_airports) || !$this->solr_core_airports->ping()) {
            $this->solr_core_airports = new Apache_Solr_Service(DB_HOST, SOLR_PORT, '/solr/amswad/');
        }

        if (!isset($criteria) || !isset($criteria->text) || strlen($criteria->text) < 2 ){
            $response = new Response("success", "Solr amswad No criteria for search.");
            $this->EncodeResponce($response);
        } else{
             $strCriteria=validate_search_string($strCriteria);
             $query = "text:".$criteria->text;
        
                if (strlen($query) > 1) {
                    // if magic quotes is enabled then stripslashes will be needed
                    
                    //$query = stripslashes($query);
                    $offset = isset($criteria->offset)?$criteria->offset:0;
                    $limit = isset($criteria->limit)?$criteria->limit:10;

                    $method = Apache_Solr_Service::METHOD_GET;
                    //&hl=true&hl.fl=aname&hl.snippets=3&hl.mergeContigious=true
                    $params = array (
                        "hl"  => "true",
                        "hl.fl" => "iata,icao,iso2,apName,cName,stName,ctName,apLabel",
                        "hl.method"   => "unified",
                        "hl.mergeContiguous"   => "true",
                        "hl.tag.pre"=>"<strong>",
                        "hl.tag.post"=>"</strong>",
                        "hl.encoder"=>"html"
                    );

                try {
                    $results = $this->solr_core_airports->search($query, $offset, $limit, $params, $method);
                    AmsAlLogger::log($mn, " response = " . json_encode($results));
                    $response = new Response("success", "Solr amswad autocomplete.");
                    $response->addData("autocomplete", $results);
                    
                } catch (Exception $ex) {
                    AmsAlLogger::logError($mn, $ex);
                    $response = new Response($ex);
                }
            }
        }
        
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AirportById($id) {
       
        $mn = "SolrClient::AirportById(".$id.")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        
        if (!isset($this->solr_core_airports) || !$this->solr_core_airports->ping()) {
            AmsAlLogger::log($mn, " DB_HOST = " . DB_HOST);
            AmsAlLogger::log($mn, " SOLR_PORT = " . SOLR_PORT);
            $this->solr_core_airports = new Apache_Solr_Service(DB_HOST, SOLR_PORT, '/solr/amswad/', $this->transportInstance);
        }

        if (!isset($id) ||  $id == ""){
            $response = new Response("success", "Solr amswad No criteria for search.");
            $this->EncodeResponce($response);
        } else{
            //$id = $id + 0;
            $query = "id:".$id;
            if (strlen($query) > 1) {
                
                //$query = stripslashes($query);
                //AmsAlLogger::log($mn, " query = " . $query);
                $offset = 0;
                $limit = 2;
                $params = array();
                $params = array (
                    "wt"  => "json",
                );
                $method = Apache_Solr_Service::METHOD_POST;
                try {
                    $this->solr_core_airports->setCreateDocuments(true);
                    $results = $this->solr_core_airports->search($query, $offset, $limit);
                   
                    $data = json_decode($results->getRawResponse());
                    //AmsAlLogger::log($mn, " rawResponce = " . serialize($data));
                    $response = new Response("success", "Solr amswad AirportById.");
                    
                    $response->addData("solr", $data);
                    if($data->response->numFound ==1){
                        $response->addData("airport", $data->response->docs[0]);
                        $response->addData("numFound", $data->response->numFound);
                    }
                    
                } catch (Exception $ex) {
                     AmsAlLogger::log($mn, " error = " . serialize($ex));
                    AmsAlLogger::logError($mn, $ex);
                    $response = new Response($ex);
                }
            }
        }
        AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
}
