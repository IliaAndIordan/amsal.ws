<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAirportRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAirportRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("UserModel.class.php");
require_once("CountryModel.class.php");
require_once("StateModel.class.php");
require_once("AirportModel.class.php");
require_once("RunwayModel.class.php");

/**
 * Description of AmsAirportRestHandler
 *
 * @author IZIordanov
 */
class AmsAirportRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "AmsAirportRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new AmsAirportRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "AmsAirportRestHandler::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            if (isset($conn)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Airport">
    
    public function AmsAirportGet($value) {
        $mn = "AmsAirportRestHandler::AmsAirportGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsAirport::LoadById($value);
            $response = new Response("success", "City get.");
            $response->addData("airport", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AmsAirportSave($value) {
        $mn = "AmsAirportRestHandler::AmsAirportSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsAirport::Save($value);
            $response = new Response("success", "Airport data saved.");
            $response->addData("airport", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AmsAirportDelete($ap_id) {
        $mn = "AmsAirportRestHandler::AmsAirportDelete(".$ap_id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = AmsAirport::Delete($ap_id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Airport deleted.");
            $response->addData("apId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Runway">
    
    public function AirportRunways($ap_id) {
        $mn = "AmsAirportRestHandler::AirportRunways(".$ap_id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $objArr = Runway::LoadByAirportId($ap_id);
            $response = new Response("success", "Airport data saved.");
            $response->addData("runways", $objArr);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function RunwayGet($value) {
        $mn = "AmsAirportRestHandler::RunwayGet(".$value.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = Runway::LoadById($value);
            $response = new Response("success", "runway get.");
            $response->addData("runway", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    
    public function RunwaySave($value) {
        $mn = "AmsAirportRestHandler::RunwaySave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = Runway::Save($value);
            $response = new Response("success", "Airport data saved.");
            $response->addData("runway", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function RunwayDelete($rw_id) {
        $mn = "AmsAirportRestHandler::RunwayDelete(".$rw_id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = Runway::Delete($rw_id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Runway deleted.");
            $response->addData("Runway id", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
}
