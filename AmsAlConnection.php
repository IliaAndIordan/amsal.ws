<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: bwt
 * Module                       : bwt
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAlConnection.php
 *
 * Database System        	: ORCL, MySQL
 * Created from			: IordIord
 * Date Creation		: 14.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 * 	 
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAlConnection.php,v $
 * <br>---
 * <br>--- 
 *
 * ******************************** HEAD_END ************************************
 */

global $amsDbConnection;
global $contentPage;

require_once("config.inc.php");
require_once("ConnectionBase.class.php");
require_once("AmsAlLogger.php");
//require_once("Functions.php");
require_once("Response.class.php");


// <editor-fold defaultstate="collapsed" desc="Connect Class">

class AmsAlConnection extends ConnectionBase{
    private $dbHost=null;
    private $dbName=null;
    private $dbUser=null;
    private $dbPassword=null;
    private $dbPort=null;
    
    //establish db connection
    public function __construct() {
        $mn = "AmsAlConnection:__construct()";
        //AmsAlLogger::logBegin($mn);
        parent::__construct(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT);
        
        $this->dbHost = DB_HOST;
        $this->dbUser = DB_USER;
        $this->dbPassword = DB_PASS;
        $this->dbName = DB_NAME;
        $this->dbPort = DB_PORT;
        try {
            //$this->connection->query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");


            // Will not affect $mysqli->real_escape_string();
            $this->connection->query("SET CHARACTER SET utf8");

            // But, this will affect $mysqli->real_escape_string();
            $this->connection->set_charset('utf8');

            $charset = $this->connection->character_set_name();
            
            //AmsAlLogger::log($mn, "Connection to ".DB_NAME." established. Charset:".$charset);
            if (mysqli_connect_errno()) {
                AmsAlLogger::logBegin($mn);
                AmsAlLogger::log("$mn", "Database connect Error : " . mysqli_connect_error($this->connection));
                AmsAlLogger::logEnd($mn);
                //header('Location: /dberror.html');
                //die();
                //header("Location: ".$url);
                //ob_flush();
            }
        } catch (Exception $ex) {
            echo 'Exception:' . $ex;
            AmsAlLogger::logBegin($mn);
            AmsAlLogger::logError($mn, $ex);
            AmsAlLogger::logEnd($mn);
        }
        //AmsAlLogger::logEnd($mn);
    }
    
    public static function dbConnect() {
        global $amsDbConnection;
        if(!isset($amsDbConnection))
        {
            $amsDbConnection = new AmsAlConnection();
        }
        
        return $amsDbConnection;
        
    }
}


// </editor-fold>
/**
 * ******************************************************************************
 *                        Iordan Iordanov 2009
 * ******************************************************************************
 * */
