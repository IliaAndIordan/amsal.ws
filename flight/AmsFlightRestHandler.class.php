<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsFlightRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsFlightRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("config.inc.php");
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("UserModel.class.php");
require_once("AirlineModel.class.php");
require_once("FlightModel.class.php");
require_once("CfgCharterModel.class.php");

/**
 * Description of AmsFlightRestHandler
 *
 * @author IZIordanov
 */
class AmsFlightRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "AmsFlightRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new AmsFlightRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "AmsFlightRestHandler::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            if (isset($conn)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Flight">
    
     public function FlightGet($value) {
        $mn = "AmsFlightRestHandler::FlightGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = FlightModel::LoadById($value);
            $response = new Response("success", "FlightModel get.");
            $response->addData("flight", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function GetLastArrTimeByAcId($value) {
        $mn = "AmsFlightRestHandler::GetLastArrTimeByAcId()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = FlightModel::LastArrTimeByAcId($value);
            $response = new Response("success", "LastArrTimeByAcId get.");
            $response->addData("lastArrTime", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="CFG Charter">
    
     public function CfgCharterGet($value) {
        $mn = "AmsFlightRestHandler::CfgCharterGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = CfgCharterModel::LoadById($value);
            $response = new Response("success", "CfgCharterModel get.");
            $response->addData("charter", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CfgCharterSave($value) {
        $mn = "AmsFlightRestHandler::CfgCharterSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = CfgCharterModel::Save($value);
            $response = new Response("success", "Charter data saved.");
            $response->addData("charter", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CfgCharterDelete($id) {
        $mn = "AmsFlightRestHandler::CfgCharterDelete(".$id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = CfgCharterModel::Delete($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "CfgCharter deleted.");
            $response->addData("charterId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CfgCharterPrecallculate() {
        $mn = "AmsFlightRestHandler::CfgCharterPrecallculate()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $id = CfgCharterModel::Precallculate();
            
            $response = new Response("success", "CfgCharter Precallculate.");
            $response->addData("status", "success");
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
    
}
