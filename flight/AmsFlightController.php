<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : AmsFlightController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/amsal' . $delim . '/home/iordanov/common/lib/amsal/common' . 
    $delim . '/home/iordanov//iordanov.info/amsal/ws'.
    $delim . '/home/iordanov//iordanov.info/amsal/ws/admin'.
    $delim . '/home/iordanov//iordanov.info/amsal/ws/airline'.
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "AmsFlightController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("AmsUserRestHandler.class.php");
require_once("UserModel.class.php");
require_once("AirlineModel.class.php");
require_once("AmsBank.class.php");
require_once("FlightModel.class.php");
require_once("AircraftModel.class.php");
require_once("CfgCharterModel.class.php");
require_once("AmsFlightRestHandler.class.php");

AmsAlLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
AmsAlLogger::log($mn, "view=" . $view);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new AmsAirportRestHandler();
    $restHendler->Option();
    AmsAlLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    
    $payloadJson;
    $payloadAuth;
    
    $authRes = JwtAuth::Autenticate();
    
    if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new AmsFlightRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    }
    else{
        $response = new Response("error", 'Autentication is required');
        $response->statusCode = 401;
        $rh = new AmsFlightRestHandler();
        $rh->EncodeResponce($response);
        return;
    }
    
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {
        // <editor-fold defaultstate="collapsed" desc="Common">
        
        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new AmsFlightRestHandler();
            $restHendler->Ping($id);
            AmsAlLogger::log($mn, "ping executed");
            break;
        
         // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="Flight">
        
        case "flights_table":
            // to handle REST Url /pcpd/
            $rh = new AmsFlightRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = FlightModel::FlightTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "flight_get":
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->FlightGet($dataJson->flId);
            }
            break;
        
         case "flights_queue_table":
            // to handle REST Url /pcpd/
            $rh = new AmsFlightRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = FlightModel::FlightQueueTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "last_arr_time":
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->GetLastArrTimeByAcId($dataJson->acId);
            }
            break;
           
        // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="Flight History">
        
        case "flights_h_table":
            // to handle REST Url /pcpd/
            $rh = new AmsFlightRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = FlightModel::FlightHistoryTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="Cfg Charter">
            
        case "ams_charter_table":
            // to handle REST Url /pcpd/
            $rh = new AmsFlightRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = CfgCharterModel::AmsCharterTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "cfg_charter_table":
            // to handle REST Url /pcpd/
            $rh = new AmsFlightRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = CfgCharterModel::CfgCharterTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "cfg_charter_get":
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CfgCharterGet($dataJson->charterId);
            }
            break;
            
        case "cfg_charter_save":
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CfgCharterSave($dataJson->charter);
            }
            break;
        
        case "cfg_charter_delete":
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CfgCharterDelete($dataJson->charterId);
            }
            break;
        case "cfg_charter_precallculate":
            $rh = new AmsFlightRestHandler();
            $rh->CfgCharterPrecallculate();
            
            break;
            
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Flight Charter">
            
        case "ac_flight_estimate_charter":
            
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            $dataJson = json_decode($payload);
            //AmsAlLogger::log($mn, "ac_flight_estimate_charter dataJson: " . json_encode($dataJson) . " ");
            if (isset($dataJson)){
                $response = FlightModel::FlightsEstimateCharter($dataJson->acId, $dataJson->apId, $dataJson->depTime);
                 //AmsAlLogger::log($mn, "response: " . json_encode($response));
                $rh->EncodeResponce($response);
                return;
            } else{
                $response = new Response("error", 'Missing required parameters.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
        case "ac_flight_estimate_charter_create":
            
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            $dataJson = json_decode($payload);
            //AmsAlLogger::log($mn, "ac_flight_estimate_charter_create dataJson: " . json_encode($dataJson) . " ");
            if (isset($dataJson)){
                $response = FlightModel::FlightsEstimateCharterCreate($dataJson->fleId);
                 //AmsAlLogger::log($mn, "response: " . json_encode($response));
                $rh->EncodeResponce($response);
                return;
            } else{
                $response = new Response("error", 'Missing required parameters.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
        // </editor-fold>   
            
        // <editor-fold defaultstate="collapsed" desc="Flight Transfer">
            
        case "ac_flight_estimate_transfer":
            
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            $dataJson = json_decode($payload);
            AmsAlLogger::log($mn, "ac_flight_estimate_transfer dataJson: " . json_encode($dataJson) . " ");
            if (isset($dataJson)){
                $response = FlightModel::FlightEstimateTransfer($dataJson->acId, $dataJson->apId);
                 AmsAlLogger::log($mn, "response: " . json_encode($response));
                $rh->EncodeResponce($response);
                return;
            } else{
                $response = new Response("error", 'Missing required parameters.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
        
        case "ac_flight_estimate_transfer_create":
            
            $rh = new AmsFlightRestHandler();
            $payload = file_get_contents('php://input');
            $dataJson = json_decode($payload);
            AmsAlLogger::log($mn, "ac_flight_estimate_transfer_create dataJson: " . json_encode($dataJson) . " ");
            if (isset($dataJson)){
                $response = FlightModel::FlightsEstimateTransferCreate($dataJson->fleId);
                 AmsAlLogger::log($mn, "response: " . json_encode($response));
                $rh->EncodeResponce($response);
                return;
            } else{
                $response = new Response("error", 'Missing required parameters.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
        // </editor-fold>    
            
        default:
            AmsAlLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


AmsAlLogger::logEnd($mn);

