<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $_aLogger;

require_once("LoggerBase.php");
Logger::configure(dirname(__FILE__) . '/appender_pdo.properties');
/**
 * Description of Log
 *
 * @author izior
 */
class AmsAlLogger extends LoggerBase
{
    
    // <editor-fold defaultstate="collapsed" desc="__construct">
    public function __construct()
    {
        parent::__construct();
        $this->MN = "AmsAlLogger: ";
        try {


            $this->logger = Logger::getRootLogger();

            $this->logger->debug("AmsAlLogger init");

        } catch (Exception $ex) {
            echo "AmsAlLogger Error: " . $ex . "<br/>";
        }
        //logEndST($MN, $ST);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function currLogger()
    {
        global $_aLogger;
        if (!isset($_aLogger)) {
            $_aLogger = new AmsAlLogger();
        }

        return $_aLogger;
    }

    public static function logBegin($mn)
    {
        AmsAlLogger::currLogger()->begin($mn);
    }

    public static function logEnd($mn)
    {
        AmsAlLogger::currLogger()->end($mn);
    }

    public static function log($mn, $msg)
    {
        AmsAlLogger::currLogger()->debug($mn, $msg);
    }

    public static function logError($mn, $ex)
    {
        AmsAlLogger::currLogger()->error($mn, $ex);
    }
    // </editor-fold>
}
