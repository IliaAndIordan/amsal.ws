<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : AmsAuthController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/amsal' . $delim . '/home/iordanov/common/lib/amsal/common' . 
    $delim .'/home/iordanov/common/lib/amsal/al-shedule' . 
    $delim . '/home/iordanov//iordanov.info/amsal/ws/admin'.
    $delim . '/home/iordanov//iordanov.info/amsal/ws/airline'.
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "AmsAuthController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("Functions.php");
require_once("AmsAuthRestHandler.class.php");
require_once("AirportModel.class.php");

AmsAlLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
AmsAlLogger::log($mn, "view=" . $view);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new AmsAuthRestHandler();
    $restHendler->Option();
    AmsAlLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAuthRestHandler();
            $restHendler->Ping($id);
            AmsAlLogger::log($mn, "ping executed");
            break;
        
        case "login":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAuthRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $location = (property_exists($dataJson, 'location'))?$dataJson->location:null;
                //AmsAlLogger::log($mn, "[login] dataJson: " . $dataJson->username . " ");
                 $restHendler->Login($dataJson->username, $dataJson->password, $location);
            }
            break;
            
        case "user_save":
            $restHendler = new AmsAuthRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $restHendler->AmsUserSave($dataJson->user);
            }
            break;
        case "user_settings":
            $restHendler = new AmsAuthRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $restHendler->AmsUserSettings($dataJson->id);
            }
            break;
            
        case "register":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAuthRestHandler();
            // read JSon input
             $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
            
                //AmsAlLogger::log($mn, "[Register] dataJson: " . $dataJson->email . " ");
                $restHendler->Register($dataJson->email, $dataJson->password, $dataJson->location);
            }
            break;
            
        case "refreshtoken":
            // to handle REST Url /pcpd/

            $restHendler = new AmsAuthRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $restHendler->refreshToken($dataJson->refresh);
            } else{
                $response = new Response("error", 'Required parameters missing in request.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
        default:
            AmsAlLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


AmsAlLogger::logEnd($mn);

