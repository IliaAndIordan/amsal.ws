<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : AmsAirlineController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/amsal' . $delim . '/home/iordanov/common/lib/amsal/common' . 
    $delim .'/home/iordanov/common/lib/amsal/al-shedule' . 
    $delim . '/home/iordanov//iordanov.info/amsal/ws'.
    $delim . '/home/iordanov//iordanov.info/amsal/ws/admin'.
    $delim . '/home/iordanov//iordanov.info/amsal/ws/airline'.
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "AmsAirlineController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("AmsUserRestHandler.class.php");
require_once("UserModel.class.php");
require_once("AirlineModel.class.php");
require_once("AmsBank.class.php");
require_once("AmsTransaction.class.php");
require_once("AmsAirlineRestHandler.class.php");
require_once("AmsHubModel.class.php");

AmsAlLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
AmsAlLogger::log($mn, "view=" . $view);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new AmsAirportRestHandler();
    $restHendler->Option();
    AmsAlLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    
    $payloadJson;
    $payloadAuth;
    
    $authRes = JwtAuth::Autenticate();
    
    if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new AmsAirlineRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    }
    else{
        $response = new Response("error", 'Autentication is required');
        $response->statusCode = 401;
        $rh = new AmsAirlineRestHandler();
        $rh->EncodeResponce($response);
        return;
    }
    
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {
    // <editor-fold defaultstate="collapsed" desc="Common">
        
        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new AmsAirlineRestHandler();
            $restHendler->Ping($id);
            AmsAlLogger::log($mn, "ping executed");
            break;
        
         // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="Airline">
        
        case "al_table":
            // to handle REST Url /pcpd/
            $rh = new AmsAirlineRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AirlineModel::AirlineTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "airline_get":
            $rh = new AmsAirlineRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AirlineGet($dataJson->alId);
            }
            break;
            
        case "airline_save":
            $rh = new AmsAirlineRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AirlineSave($dataJson->airline);
            }
            break;
        
        case "airline_delete":
            $rh = new AmsAirlineRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AirlineDelete($dataJson->alId);
            }
            break;
        
        case "airline_logo":
            $rh = new AmsAirlineRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AirlineLogoCreate($dataJson->alId);
            }
            break;
            
        case "airline_livery":
            $rh = new AmsAirlineRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->AirlineLiveryCreate($dataJson->alId);
            }
            break;
            
        case "airline_bank_h30days":
            $rh = new AmsAirlineRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->BankHistory30DaysJson($dataJson->alId);
            }
            break;
           
        // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="Airline Hub">
            
        case "airline_hub_get":
        $rh = new AmsAirlineRestHandler();
        $payload = file_get_contents('php://input');

        if (isset($payload)){
            $dataJson = json_decode($payload);
            $rh->HubGet($dataJson->hubId);
        }
        break;

        case "airline_hub_save":
            $rh = new AmsAirlineRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->HubSave($dataJson->hub);
            }
            break;
        
        case "airline_hub_delete":
            $rh = new AmsAirlineRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->HubDelete($dataJson->hubId);
            }
            break;
            
        case "airline_hub_table":
             // to handle REST Url /pcpd/
            $rh = new AmsAirlineRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsHubModel::AmsHubsTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Tansaction">
            
        case "al_transaction_table":
            // to handle REST Url /pcpd/
            $rh = new AmsAirlineRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsTransaction::AmsTransactionTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "al_transaction_sum_last_week":
            // to handle REST Url /pcpd/
            $rh = new AmsAirlineRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsTransaction::AmsTransactionSumByDateLastWeek($payload_json->alId);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "al_transaction_sum_today":
            // to handle REST Url /pcpd/
            $rh = new AmsAirlineRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsTransaction::AmsTransactionSumToday($payload_json->alId);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "al_trans_stat_lst_8_weeks":
             // to handle REST Url /pcpd/
            $rh = new AmsAirlineRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsTransaction::AmsTransactionAlStWeekTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "al_revenues_table":
             // to handle REST Url /pcpd/
            $rh = new AmsAirlineRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //AmsAlLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = AmsTransaction::AmsStRevenuesDayTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        // </editor-fold>    
            
        default:
            AmsAlLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


AmsAlLogger::logEnd($mn);

