<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAirlineSheduleRestHandler.class.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAirlineSheduleRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("config.inc.php");
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("UserModel.class.php");
require_once("AirlineModel.class.php");
require_once("AmsHubModel.class.php");
require_once("AmsAlSheduleGroup.class.php");
require_once("AmsAlSheduleRoute.class.php");

/**
 * Description of AmsAirlineSheduleRestHandler.class
 *
 * @author IZIordanov
 */
class AmsAirlineSheduleRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "AmsAirlineSheduleRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new AmsAirlineSheduleRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "AmsAirlineSheduleRestHandler::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            if (isset($conn)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="AmsAlSheduleGroup">
    
     public function alSheduleGroupGet($value) {
        $mn = "AmsAirlineSheduleRestHandler::alSheduleGroupGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsAlSheduleGroup::LoadById($value);
            $response = new Response("success", "AmsAlSheduleGroup get.");
            $response->addData("group", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function alSheduleGroupSave($value) {
        $mn = "AmsAirlineSheduleRestHandler::alSheduleGroupSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsAlSheduleGroup::Save($value);
            $response = new Response("success", "Airline group saved.");
            $response->addData("group", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function alSheduleGroupDelete($grpId) {
        $mn = "AmsAirlineSheduleRestHandler::alSheduleGroupDelete(".$grpId.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = AmsAlSheduleGroup::Delete($grpId, $conn, $mn, $logModel);
            
            $response = new Response("success", "Airline group deleted.");
            $response->addData("grpId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="AmsAlSheduleRoute">
    
     public function alSheduleRouteGet($value) {
        $mn = "AmsAirlineSheduleRestHandler::alSheduleRouteGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsAlSheduleRoute::LoadById($value);
            $response = new Response("success", "AmsAlSheduleRoute get.");
            $response->addData("route", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function alSheduleRouteSave($value) {
        $mn = "AmsAirlineSheduleRestHandler::alSheduleRouteSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsAlSheduleRoute::Save($value);
            $response = new Response("success", "Airline group saved.");
            $response->addData("route", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function alSheduleRouteDelete($routeId) {
        $mn = "AmsAirlineSheduleRestHandler::alSheduleRouteDelete(".$routeId.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = AmsAlSheduleRoute::Delete($routeId, $conn, $mn, $logModel);
            
            $response = new Response("success", "Airline route deleted.");
            $response->addData("routeId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
}
