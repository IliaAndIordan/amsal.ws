<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsAirlineRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsAirlineRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("config.inc.php");
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("AmsAlConnection.php");
require_once("AmsAlLogger.php");
require_once("JwtAuth.php");
require_once("Functions.php");
require_once("UserModel.class.php");
require_once("AirlineModel.class.php");
require_once("AmsHubModel.class.php");

/**
 * Description of AmsAirlineRestHandler
 *
 * @author IZIordanov
 */
class AmsAirlineRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "AmsAirlineRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new AmsAirlineRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "AmsAirlineRestHandler::Ping()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            if (isset($conn)) {
                AmsAlLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Airline">
    
     public function AirlineGet($value) {
        $mn = "AmsAirportRestHandler::AirlineGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AirlineModel::LoadById($value);
            $response = new Response("success", "AirlineModel get.");
            $response->addData("airline", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AirlineSave($value) {
        $mn = "AmsAirportRestHandler::AirlineSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AirlineModel::Save($value);
            $response = new Response("success", "Airline data saved.");
            $response->addData("airline", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AirlineDelete($alId) {
        $mn = "AmsAirportRestHandler::Airline(".$alId.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = AirlineModel::Delete($alId, $conn, $mn, $logModel);
            
            $response = new Response("success", "Airline deleted.");
            $response->addData("alId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AirlineLogoCreate($value) {
        $mn = "AmsAirportRestHandler::AirlineLogoCreate()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $dataJson = AirlineModel::LoadById($value);
            $al = AirlineModel::fromJSON($dataJson);
             //AmsAlLogger::log($mn, " al = " . json_encode(json_decode($al)));
             $logoUrl = $al->getLogoPath();
            $response = new Response("success", "AirlineModel Get Logo.");
            $response->addData("airline", $al);
            $response->addData("logoUrl", $logoUrl);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function AirlineLiveryCreate($value) {
        $mn = "AmsAirportRestHandler::AirlineLiveryCreate()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $dataJson = AirlineModel::LoadById($value);
            $al = AirlineModel::fromJSON($dataJson);
             //AmsAlLogger::log($mn, " al = " . json_encode(json_decode($al)));
             $logoUrl = $al->LiveryPath();
            $response = new Response("success", "AirlineModel Get Logo.");
            $response->addData("airline", $al);
            $response->addData("liveryUrl", $logoUrl);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="AmsHubModel">
    
     public function HubGet($value) {
        $mn = "AmsAirportRestHandler::HubGet()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsHubModel::LoadById($value);
            $response = new Response("success", "AirlineModel get.");
            $response->addData("hub", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function HubSave($value) {
        $mn = "AmsAirportRestHandler::HubSave()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = AmsHubModel::Save($value);
            $response = new Response("success", "Airline data saved.");
            $response->addData("hub", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function HubDelete($hubId) {
        $mn = "AmsAirportRestHandler::HubDelete(".$hubId.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = AmsHubModel::Delete($hubId, $conn, $mn, $logModel);
            
            $response = new Response("success", "Airline deleted.");
            $response->addData("hubId", $id);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Bank History Chart">
    
    function BankHistory30DaysJson($alId){
        $mn = "AmsAirlineRestHandler::BankHistory30DaysJson()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
             $sql = "SELECT round(amount, 2) as amount, DATE(adate) adate
                    FROM ams_al.ams_bank_h h                    
                    join (SELECT id
                            FROM ams_al.ams_bank_h
                            where al_id=?
                            ORDER BY id DESC LIMIT 30) s on s.id = h.id
                    where h.al_id=? 
                    order by h.id " ;

            $bound_params_r = ["ii",$alId, $alId];

            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("rows", $ret_json_data);
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
}
